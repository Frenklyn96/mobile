
local composer = require( "composer" )

local funList = {}

-- local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

-- Gruppi di visualizzazione
local backGroup 	--Background
local mainGroup		--Foreground
local uiGroup		--UI

local lives
local score
local livesText
local scoreText

local function createUI()

	lives = 3
	score = 0
 	--Creazione testo
	livesText = display.newText("Lives: " .. lives, 200, 80, native.systemFont, 36)
 	scoreText = display.newText("Scores: " .. score, 400, 80, native.systemFont, 36)

	-- Sort everything in the correct z-index order
	local stage = display.getCurrentStage()
	stage:insert( composer.stage )
	stage:insert(livesText)
	stage:insert(scoreText)
	
end

funList.createUI = createUI

local function updateText()
    livesText.text = "Lives: " .. lives
    scoreText.text = "Score: " .. score
end

return funList
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()
-- function scene:create( event )

-- 	local sceneGroup = self.view
-- 	-- Code here runs when the scene is first created but has not yet appeared on screen
	
-- 	uiGroup = display.newGroup()
-- 	sceneGroup:insert(uiGroup)

-- 	--Creazione testo
-- 	livesText = display.newText(uiGroup, "Lives: " .. lives, 200, 80, native.systemFont, 36)
-- 	scoreText = display.newText(uiGroup, "Scores: " .. score, 400, 80, native.systemFont, 36)


-- end


-- -- show()
-- function scene:show( event )

-- 	local sceneGroup = self.view
-- 	local phase = event.phase

-- 	if ( phase == "will" ) then
-- 		-- Code here runs when the scene is still off screen (but is about to come on screen)


-- 	elseif ( phase == "did" ) then
-- 		-- Code here runs when the scene is entirely on screen

-- 	end
-- end


-- -- hide()
-- function scene:hide( event )

-- 	local sceneGroup = self.view
-- 	local phase = event.phase

-- 	if ( phase == "will" ) then
-- 		--viene eseguito quando la scena è sullo schermo(ma sta per andare fuori schermo)

-- 	elseif ( phase == "did" ) then
-- 		--il codice qui viene eseguito immediatamente dopo che la scena va interamente fuori schermo

-- 	end
-- end


-- -- destroy()
-- function scene:destroy( event )

-- 	local sceneGroup = self.view
	
--     --il codice qui viene eseguito prima della rimozione della vista della scena

-- end


-- -- -----------------------------------------------------------------------------------
-- -- Scene event function listeners
-- -- -----------------------------------------------------------------------------------
-- scene:addEventListener( "create", scene )
-- scene:addEventListener( "show", scene )
-- scene:addEventListener( "hide", scene )
-- scene:addEventListener( "destroy", scene )
-- -- -----------------------------------------------------------------------------------

-- return scene

