--sample main.lua code:
local joyStickLib = require( "simpleJoystick" )
local joyStick = joyStickLib.new( 50, 100 )

-- Lista di funzioni restituite dal file
local FunList = {}

-- Prima immagine
local backGround1
-- Seconda immagine
local backGround2
-- 
local runtime = 0
-- 
local scrollSpeed = 1.4


local group_background = display.newGroup()
local group_foreground = display.newGroup()

-- Carica le due immagini
local function addScrollableBackGround()
    local backGroundImage = { type="image", filename="background.jpg" }

    -- Load prima immagine
    backGround1 = display.newRect(group_background, 0, 0, display.contentWidth, display.actualContentHeight)
    backGround1.fill = backGroundImage
    backGround1.x = display.contentCenterX
    backGround1.y = display.contentCenterY

    -- Load seconda immagine
    backGround2 = display.newRect(group_background, 0, 0, display.contentWidth, display.actualContentHeight)
    backGround2.fill = backGroundImage
    backGround2.x = display.contentCenterX
    backGround2.y = display.contentCenterY - display.actualContentHeight
end

-- Aggiunta alla lista di funzioni di ritorno FunList la funzione addScrollableBackGround  
FunList.addScrollableBackGround = addScrollableBackGround

-- Muove le due immagini
local function moveBackGround(dt)
	-- Sposta la prima immagine in su 
    backGround1.y = backGround1.y + scrollSpeed * dt
    -- Sposta la seconda immagine in su
    backGround2.y = backGround2.y + scrollSpeed * dt

    -- Se la prima immagine raggiunge una altezza non più utile rimandala sotto
    if (backGround1.y - display.contentHeight/2) > display.actualContentHeight then
        backGround1:translate(0, -backGround1.contentHeight * 2)
    end
    -- Se la seconda immagine raggiunge una altezza non più utile rimandala sotto
    if (backGround2.y - display.contentHeight/2) > display.actualContentHeight then
        backGround2:translate(0, -backGround2.contentHeight * 2)
    end
end

-- CONTROLLO: SERVONO? /TODO
-- 
-- local function getDeltaTime()
   
--    local temp = system.getTimer()
--    local dt = (temp-runtime) / (1000/60)
--    runtime = temp
--    return dt
-- end

-- local function enterFrame()
--     local dt = getDeltaTime()
--     moveBackGround(dt)
-- end

-- local function movimento()
-- 	moveBackGround(10)
-- end


----------

joyStick.x = display.contentWidth-display.contentWidth/4
joyStick.y = display.contentHeight
group_foreground:insert(joyStick)

--local mainCharacter= display.newCircle( group_foreground, display.contentCenterX, display.contentCenterY, 40 )
local mainCharactera= require("mainCharacter")
local mainCharacter = mainCharactera.new()

--mainCharacter:setFillColor( 0,0,0 )

local salvaDirezione = 0

function catchTimer()
	print( "  joystick info: " .. " dir=" .. joyStick:getDirection() .. " angle=" .. joyStick:getAngle() .. " dist="..joyStick:getDistance() )
		
	if (joyStick.getDirection() ~= 0) then
		mainCharacter.x = (mainCharacter.x + math.cos( math.rad(joyStick:getAngle()) )*10*joyStick:getDistance())
		mainCharacter.y = (mainCharacter.y - math.sin( math.rad(joyStick:getAngle()) )*10*joyStick:getDistance())
		salvaDirezione= joyStick:getDirection()

	elseif(salvaDirezione ~= 0 and mainCharacter.y==display.contentCenterY) then
		mainCharacter.y = mainCharacter.y+50
		--moveBackGround(-50)
		salvaDirezione = 0
	end

	if(mainCharacter.y<display.contentCenterY) then
		moveBackGround(display.contentCenterY-mainCharacter.y)
		mainCharacter.y=display.contentCenterY
	end

	return true
end


joyStick:activate()

addScrollableBackGround()
--timer.performWithDelay( 500, catchTimer, -1 )
Runtime:addEventListener( "enterFrame", catchTimer)
Runtime:addEventListener( "enterFrame", mainCharacter.edgeScreenWrap)



-- Restituzione lista di funzioni di ritorno 
return FunList