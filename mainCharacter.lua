local maincharacter={}
-- Permette al giocatore, una volta raggiunto un bordo, di passare dall'altra parte

function maincharacter.new()

	local mainCharacter=display.newImageRect("Dude.png",50,150)
	mainCharacter.x=display.contentCenterX
	mainCharacter.y=display.contentHeight


	function mainCharacter:edgeScreenWrap()
		if(mainCharacter.x>display.contentWidth)then

			mainCharacter.x=0
		elseif(mainCharacter.x<0)then

			mainCharacter.x=display.contentWidth

		elseif(mainCharacter.y>display.contentHeight+100)then

			mainCharacter.y=display.contentHeight+100
		end
	end

	return(mainCharacter)
end

return maincharacter