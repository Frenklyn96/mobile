local composer = require("composer")

-- Nasconde la status bar
display.setStatusBar(display.HiddenStatusBar)

-- Cambio seed per numeri pseudocasuali 
math.randomseed( os.time())

-- Audio, canale riservato 1 
-- audio.reserveChannels(1)
-- audio.setVolume(0.5, {channel = 1})

composer.gotoScene("menu")

